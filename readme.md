# Formbuilder

A simple PHP class for outputting form elements. Lightweight, no dependencies, ready to include in your project.

## Usage

```
$formbuilder = new Apsis\Formbuilder();
echo $formbuilder->label('input_name', 'Label Text');
echo $formbuilder->input('text', 'input_name');
```