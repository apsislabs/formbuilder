<?php namespace Apsis;

class Formbuilder {

    public function formStart($action = '', $options = array()) {
        $merge = compact('action');
        $options = array_merge($options, $merge);

        return sprintf('<form %s>', $this->attributes($options));
    }

    public function formEnd() {
        return sprintf('</form>');
    }

    public function label($name, $value = null, $options = array()) {
        $value = $value ?: ucwords(str_replace('_', ' ', $name));
        return sprintf('<label for="%s" %s>%s</label>', $name, $this->attributes($options), $value);
    }

    public function input($type, $name, $value = null, $options = array()) {
        $merge = compact('type', 'name', 'value');
        $options = array_merge($options, $merge);
        return sprintf('<input %s>', $this->attributes($options));
    }

    public function textarea($name, $value = null, $options = array()) {
        $merge = compact('name');
        $options = array_merge($options, $merge);

        return sprintf('<textarea %s>%s</textarea>', $this->attributes($options), $value);
    }

    public function checkbox($name, $value = 1, $checked = null, $options = array()) {
        return $this->checkable('checkbox', $name, $value, $checked, $options);
    }

    public function radio($name, $value = null, $checked = null, $options = array()) {
        if ( is_null($value) ) { $value = $name; }
        return $this->checkable('radio', $name, $value, $checked, $options);
    }

    public function select($name, $list = array(), $selected = null, $options = array()) {
        $merge = compact('name');
        $options = array_merge($options, $merge);

        $html = array();

        foreach ($list as $value => $display) {
            if ( is_null($value) ) { $value = $display; }
            $html[] = $this->option($display, $value, $selected);
        }

        return sprintf('<select %s>%s</select>', $this->attributes($options), implode('', $html));
    }

    public function option($display, $value = null, $selected = null) {
        $selected = $this->selected($value, $selected);
        $options = compact('value', 'selected');

        return sprintf('<option %s>%s</option>', $this->attributes($options), $display);
    }

    protected function checkable($type, $name, $value, $checked, $options) {
        if ( $checked ) { $options['checked'] = 'checked'; }
        return $this->input($type, $name, $value, $options);
    }

    protected function attributes(array $attributes) {
        $html = array();

        // Add Attributes to HTML Array
        foreach ($attributes as $key => $value) {
            if ( ! is_null($value) ) {
                $attr = is_numeric($key) ? $value : $key;
                $element = sprintf('%s="%s"', $attr, $value);
                $html[] = $element;
            }
        }

        // Implode and Return
        return ( count($html) > 0 ) ? implode(' ', $html) : '';
    }

    protected function selected($value, $selected) {
        return ((string) $value === (string) $selected) ? 'selected' : null;
    }
}